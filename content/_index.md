---
title: Introduction
BookToC: false
type: docs
---

# Something Like Theoretical Physics
{{< columns >}} <!-- begin columns block -->

![A picture of me](profile.jpg)

<---> <!-- magic separator, between columns -->

I am a theoretical physics PhD student at the Univerity of Birmingham, working under the supervision of [Professor Mark Dennis](https://markrdennis.com/).

I part of the [theoretical physics group](https://more.bham.ac.uk/theoretical-physics/) and [CDT in *Topological Design*](https://www.birmingham.ac.uk/university/colleges/eps/study/phd/cdt/topological-design-cdt/index.aspx). 

I study the relationship between *geometry*, *topology* and *quantisation* and physical systems which show how these three interact in new and interesting ways. I also love optics, laser beams and special functions.
<---> <!-- magic separator, between columns -->


Currently my favourite Hamiltonians are

{{< katex display >}}
H_{\pm}=\frac{1}{2}\left(p_x^2+p_y^2\pm q_x^2\pm q_y^2\right)
{{< /katex >}}

and

{{< katex display >}}
H_{\mathcal{Z}}=(p_x^2+p_y^2)-(xp_x+yp_y)^2+2i(xp_x+yp_y)
{{< /katex >}}

{{< /columns >}}
I am also avaliable to tutor physics and maths at any level up to selected univerity courses. Please contact me if you are intereted.
---
title: "Research"
weight: 2
---
![Some 2DSHO Caustics](headline.png)
# Research Interests
My research centers around the use of symmetry techniques in optics. Many of the symmetry methods familiar from undergraduate mechanics like continuous symmetries and conserved quantities have powerful generalisations to the idea of group actions on different kind of manifold and become applicable to many theories including optics. I use this geometry to investigate the relationship between ray and wave optics, primarily using the tools of [geometric quantization](https://ncatlab.org/nlab/show/geometric+quantization) and [semiclassical physics](https://ncatlab.org/nlab/show/semiclassical+approximation). Optical physics provides the systems and physical motication for my work. The physical pictures of the quantization of such systems is performed under the auspices of the correspondance between classical wave optics and 2+1D quantum mechanics described by the [paraxial approximation](https://en.wikipedia.org/wiki/Helmholtz_equation#Paraxial_approximation).

More explicitly I am interested in applications of geometrical methods to the quantization of pathological systems, such as those with noncompact Bohr-Sommerfeld leafs and also what the extra geometry such methods furnish (like Lagrangian foliations) can help us say about caustic singularities in phase sapace. At the moment I am trying to construct quantizations of harmonic potentials in 2D and use them to interperete various nice properties that emerge from the caustics of the semiclassical geometry.

## Tools
The tools I spend the most time playing around with and some of their standard refernce texts are
+ Optics
    + Born & Wolf - [*Principles of Optics*](https://www.cambridge.org/core/books/principles-of-optics/9D54D6FF0317074912CB285C3FF7341C)
    + Gbur - [*Singular Optics*](https://www.taylorfrancis.com/books/mono/10.1201/9781315374260/singular-optics-gregory-gbur)

+ Geometrical (Hamiltonian) Mechanics
    + Abraham & Marsden - [*Foundations of Mechanics*](https://bookstore.ams.org/chel-364.h)
    + Marsden & Ratiu - [*Introduction to Mechanics and Symmetry*](https://link.springer.com/book/10.1007/978-0-387-21792-5)

+ Geometric Quantization
    + Woodhouse - [*Geometric Quantization*](https://global.oup.com/academic/product/geometric-quantization-9780198502708?cc=gb&lang=en&)
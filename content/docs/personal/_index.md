---
title: "About Me"
weight: 1
BookToC: false
---

# About Me

I entered the University of Glasgow in 2016 to study computational physics, but soon discovered that I enjoyed university mathematics much more than in High school and so switched to theoretical physics over the summer of my 1st year. I was primarily interested in particle physics through my degree, writing projects  quark-gluon plasma in my 2nd year and electroweak symmetry breaking and extensions to the Higgs mechanism in my 4th year. In my final year I decided I didn't want to pursue a PhD in high-energy theory so I wrote my masters thesis *"Optics in curved spaces"*  with Johannes Courtial. Now I work on similar themes, those of optics and geometry, in my PhD at the University of Birmingham where I am part of the [theoretical physics group](https://more.bham.ac.uk/theoretical-physics/) and [CDT in *Topological Design*](https://www.birmingham.ac.uk/university/colleges/eps/study/phd/cdt/topological-design-cdt/index.aspx). Currently I am in the 3rd year of my PhD, I am supervised by [Professor Mark Dennis](https://markrdennis.com/). For more about my research look [here]({{< relref "../pages" >}})

## Travel

For conference travel see sidebar



## Academic History
**2021-2025:** PhD in Theoretical Physics, Univerity of Birmingham
+ **2022-2023:** [SPIE Photonics Champion](https://www.birmingham.ac.uk/news/2021/new-champions-academy-at-university-of-birmingham-for-light-based-technologies-to-tackle-pandemics-climate-change-and-brain-ageing), Univerity of Birmingham [Quantum Hub](https://www.birmingham.ac.uk/research/heroes/quantum-technologies.aspx)
+ **Summer 2022:** University of Glasgow Visiting student


**2016-2021:** MSci Theoretical Physics with Honours of the First Class, University of Glasgow
+ **2021:** Summer Internship - Optics Group, Univerity of Glasgow
+ **Masters Thesis (5th Year):** Optics in Curved Spaces, supervised by [Johannes Courtial](https://www.gla.ac.uk/schools/physics/staff/johannescourtial/)
+ **Literature Project (4th Year):**  Generic Higgs Mechanism Extensions, supervised by [David Miller](https://www.gla.ac.uk/schools/physics/staff/davidmiller/)
+ **2019:** Summer Internship - *"An Improved Model for Calculating Optimal Undulator Gaps on Hard X-ray Beamlines"*, Diamond Light Source

**2011-2016:** SQA Highers AAAAB, Carrick Academy

## Graduate Courses Taken
**Physics**
+ General Relativity and Gravitation
+ Dynamics, Electrodynamics & Relativity
+ Physics M Project
+ Problem Solving Workshop
+ Relativistic Quantum Fields
+ Statistical Mechanics
+ Quantum Information
+ Electromagnetic Theory
+ Groups And Symmetries

**Mathematics**
+ Introduction to Topology and its Applications
+ Advanced Differential Geometry and Topology

## Technical Skills
+ Linux
+ Python Programming
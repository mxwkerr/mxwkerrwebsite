---
title: "Interesting Links"
weight: 5
---

# Academics

## Learning Resources
### The [`nLab`](ncatlab.org)
### The living textbook on [`homotopy type theory`](https://homotopytypetheory.org/book/)
### The almighty [`arXiv`](https://arxiv.org/)

## Essays
### [`Lockhart's Lament`](https://www.maa.org/external_archive/devlin/LockhartsLament.pdf)
### Steven G. Krantz's [`How to Write Your First Paper`](https://www.ams.org/notices/200711/tx071101507p.pdf)

## Tools
### [`Quiver`](https://q.uiver.app/) - a commutative diagram editor

# Computers 
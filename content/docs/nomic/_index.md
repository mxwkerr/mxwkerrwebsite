---
BookToC: false
title: Nomic
---
# *Theoretically A Nomic*

{{< columns >}}
## Game Status
{{< hint warning >}}
**The nomic is dead, long live the nomic**
{{< /hint >}}

Last Updated: 28/04/2024
<--->
## [Rule Actions Archive](ammendment_archive)
## [Definitions](definitions)



<---> 
## [Current Rules](master_rules)

{{< /columns >}}

## Player Point Totals
- epsilon_nought: 11
- **SalmonCannon**: 31
- Dylan: 41
- Luke: 0
- Mr Speaker: 0

## Take Part
Nomic is a game where changing the rules are the moves. Learn more [here](https://www.nomic.net/)
If you wish to take part in the game please contact mxwkerr{at}gmail{dot}com

## <p style="text-align: center;"> Current Consideration</p>
This nomic will be archived shortly
---
BookToC: false
bookHidden: true
---

# *Theoretically A Nomic* Master Rules Document Proposal
<!-- {{< hint info >}}
These rules were concieved on the eve on the 6th night of the 6th month of 2024 and developed to be presented at the first grand nomic council of. It is designed to solve a number of problems with the first great game of nomic, the primary of which being the lack of gameplay structure with to take effective actions, resolve disutes and serve as a foundation for more mechanics.
{{< /hint >}} -->
<img src="theboard.png" alt="heboard" width="800"/>





## `[0]` The Supreme Laws Of Nomic
- `[0.1]` All players must always abide by all the rules currently in effect and conduct gameplay using the mechanics of the rules.
- `[0.2]` The supreme aim of players in the Nomic is to win, as per `X.Y`. To loose the Nomic is the worst possible fate.
- `[0.3]` A number of words or concepts are refered to in the rules using bold text. These rule should be interpreted using the description
description of the emboldend text as it appears in the definitions NRD. It is intended that these definitions define the bold terms.
- `[0.4]` Players should conduct their business in the game according to **common decency**, **morality** and the **spirit of the game**.
- `[0.5]` If a player wishes to resolve a rule or conduct related dispute, **morality** compells you to invoke the judgment mechanic (Rule `[3]`)
- `[0.6]` All gameplay materials mays be easily accessible to all players at all times.
- `[0.7]` No rule changes may not have retroactive effects and may not take effect earlier than the moment of the completion of the vote that adopted it, even if their wording explicitly states otherwise.


## `[1]` The Structure of The Nomic

### `[1.1]` The Sturucture of the rules

- `[1.1.1]` The rules of Nomic consist exclusively of the contents of this document and other permissible material the document admits.
- `[1.1.2]` Entries (of the document) consisting of an emboldednd heading denote collections of rules.
- `[1.1.3]` Entries beginning with a single bullet point are considered a rule.
    - Rules can have subpoints to aid clarity
    - The main body of a rule and all its subpoints is considered a single rule
- `[1.1.4]` Each entry has a string of integers intended to denote its position. Rules exist in the document in positions and orders consistent with their heading, in ascending order.
    - These numers are refered to as **ordinal** and are are considered part of the rule
    - The position of a rule in the document may be changed by ammending its **ordinal**
- `[1.1.5]` Changing the **ordinal** of an entry also changes the relevent components of the ordinals of all its subentrys.


### `[1.2]` The Sturucture of the turn
#### `[1.2.1]` Turn Progression
- `[1.2.1.1]` Players shall take turns in the order (top to bottom) specified by the turn order list NRD.
    - Every turn must be completed in its entirety.
    - Once the active player has finished their turn it becomes the next player’s turn.
    - Each player may only be included in the turn order list once.
    - The player currently taking a turn is referred to as the active player.
    - The turn order list will specify which player is the active player. The initial position of a player in the turn order list is at the bottom at time of joining.
- `[1.2.1.2]` When the game begins the turn order list is randomised and the player at the top becomes the active player.
- `[1.2.1.3]` A turn consists of two actions, taken in order: A rules proposition (`[4.2]`) and a rolling of the dice (`[4.3]`). All immediate gameplay consequences of a game action must be completed before the turn moves forward to the next action.


## `[3]` Judgment
- `[3.1]` Any player may invoke a judgment at any time in reference to a game action which has just occured. Judgment may not be invoked on a action after a gameplay has progressed beyod that action.
- `[3.2]` The player inoking Judgment is referred to as the *plaintiff*, the taget player the *defendent*
- `[3.2]` There are a number of types of judgement outlined below, each type has a name (so it may be refernced), a criterion (the purpous of the judgment will be to assess if this criterion is met) and two possible actions taken at the conclusion of judgment (one each for a finding in favour of the prosecution or the defendant). Judgments must find in favour of one of the parties. Judgments may also accout for aggrevateing factors. 

    - [3.2.1] *Section 1 Judgemnt* (Rules Violation).
        - Criterion: *Has the defendent blatently violated a specific rule (all recourse is of course to `[0.1]` but the defendednt must specify the immediately broken rule)*
        - Plaintiff's favour: The gameplay action on which judgment was invokes is nullified and ended. If the violation was intentional and blatent thet player also may be forced to skip their next turn.
        - Defendent's favour: The plaintiff is subject to major sanctioned as described by `[3.3.2]`

    - [3.2.1] *Section 2 Judgemnt* (Rule Ambiuguity).
        - Criterion: *Has the defendent employed an interpretation or ambiguity of a number of the gameplay rules which players disagree with, but which does not abmount to a violation of `[0.1]`*
        - Plaintiff's favour: The gameplay action on which judgment was invoked resolves as the defendant intended in their original motion, the defendent recieves minor sactions as described in `[3.3.1]`, the Judge may take a Hollow *[4.4.5] - Clarification* action which must aim to close the loophole exploited by the defendent in a minimal and clean fashion. 
        - Defendent's favour: The plaintiff is subject to minor sanctioned as described by `[3.3.1]` and the Defendent's action resolves as intended. The Judge then takes a Hollow*[4.4.5] - Clarification*  action which must aim to clarify the rules in question so that their interpretation is that of the outcome of the judgment.

    - [3.2.1] *Emergency Section 2 Judgement* (Coup de grâce attempt).
        - Criterion: *Has the defendent employed an interpretation or ambiguity of a number of the gameplay rules which players disagree with, which would cause them to wind the game if the action resolved, but which does not abmount to a violation of `[0.1]`*?
        - Plaintiff's favour: The gameplay action on which judgment was invoked is nullified and ended, the defendent recieves minor rewards as described in `[3.3.3]`, the Judge must take a Hollow *[4.4.5] - Clarification* action which must aim to close the loophole exploited by the defendent in a minimal and clean fashion. The Judge recieves none of the ususal benifits from this action.
        - Defendent's favour: The defendent's action resolves as intended.


### `[3.3]` Sanctions and Rewards
- `[3.3.1]` Minor sanctions consist in rolling a **game die** and moving that number of spaces back on **the board**
- `[3.3.2]` Major sanctions consist in moving backwards X spaces on the **the board** where X is twice the lagrest possile roll on the **game dice**
- `[3.3.3]` Minor rewards consist in rolling a **game dice** and moving forwards that number of spaces on **the board**
- `[3.3.4]` Major rewards consist in moving forwards X spaces on the **the board** where X is twice the lagrest possile roll on the **game dice**


## `[4]` Nomic Game Actions
- `[4.1]` The only way that players may interact with this document, the NRD or other permissable game materials is by using action described in this section, where other rules allow. *"Taking a game action"* consists in choosing a `[4.XX]` heading and executing the steps is describes subject to the constraints it imposes. 
- `[4.2]` The steps taken by players to fulfill the setps in actions taken must be performed in accordance with the specific (or otherwise standard or common practice) mechanisms, tools and formatting decsribed in `[X.Y]` - *Operation Rules*

### `[4.3]` Rules Proposition
A player who is performing a *rules proposition* perform the following actions in their entirety, in order:
- Submit a document called a *proposition* containing a body of text describing either the creation, deletion or ammendment of a single rule. The proposition (not the rule) should also have a title
- A vote is called, as per `[4.5]` on whether or not to accept the rule.
    - If accepted the rule takes effect, is entred into this document, the payer recieves minor rewards (see `[3.3.3]`) if this action was not part of a *clarification* and this game action concludes.
    - If defeated, a **reasonable time** is permitted to debate and discuss the proposal. At the conclusion of which the player will submit another proposal which is then voted on again. If this vote is defeated no rules change takes place and this game action ends.

### `[4.4]` Voting 
- `[4.4.1]` Votes are only passed by unanimous agreement among player who have voted.
- `[4.4.2]` The contents of each vote must be acessible to all players.
- `[4.4.3]` All players get 1 vote.
- `[4.4.3]` Votes are conducted in public.
- `[4.4.3]` Votes may be performed asynchronously.
- `[4.4.4]` A vote consists of the following actions performed in their entirety, in order:
    - The relevent material or question to be voted on is put to all players.
    - Players have a **short time** to cast their votes.
    - The outcome (*accepted* or *defeated*) is computed using `[4.4.1]`.
    - All players who did not vote have `[3.3.1]` - *Standing Judgment: Failure to vote* invoked on them.
- `[4.4.5]` If at any point during the vote, all players have voted, the outcome of the vote is immediately evaluated.

### `[4.5]` Clarification
A player who is performing a *clarification* performs the following actions in their entirety, in order:
- If the clarification is not triggerd by a judgement: submit a document, refered to as the *manifesto*, detailing the rational for the clarification. The manifesto must spectify what single mechanic or problem or rule the clarification is aimed at solving.
- Propose a serise of *Rules Proposiaiton* actions, in parallel, which would act in accordance with the manifesto
- A vote is held
    - If the vote passes, each if the proposed *Rules Proposiaiton* actions is enacted as a *Hollow Ruls Proposition* without vote. The game action ends
    - If defeated, the game action ends.

### `[4.6]` *Hollow* gameplay actions
A hollow gameplay action is any of the above gameplay actions with the additional stipulation that the player enacting them does not recieve any of the normal rewards of the action.



## `[5]` The Board Born Of The Nomic
- `[5.1]` Players may only make moves on **the board** or modify the board when a rule allows. Players may only edit their own board position NRD.
- `[5.2]` Spaces on the board are referenced using the numer written on them.
- `[5.3]` All players have a player board position NRD, which starts at 1, representig their place on the board.
- `[5.4]` When a player moves forward/backwards X spaces on **the board**, their board position NRD in incrememnted or decreased by X.
- `[5.5]` Whan a player's NRD changes, consult **the board**, if the space on the board corresponding to their NRD contains some instruction, the player must now carry out that instruction to the best of their ability while repecting `[0.1]`
- `[5.6]` *Victory Condition* - The first player to have their board position NRD be 100 becomes the winner



## `[6]` Operational Rules
- `[6.1]` All nomic gameplay is conducted in English
- `[6.2]` All documents and gameplay materials that are required to be submitted over the course of the game must be of reasonable length and formatting as befits their purpous.

## `[7]` Non-Rule Data
Players and rules may interact with information external to the ruleset; this rule facilitates the indexing and existence of such information.
- `[7.1]` Rule Data is any subset of the Universe currently contained wholly and constructively within the logical span of the current set of Rules
- `[7.2]` Non-Rule Data (NRD) is the complement of Rule Data
- `[7.3]` NRD is considered 'in scope' or 'scoped' iff it meets the following conditions:
    - Consists of media or information intended for use within this Nomic, and a unique identifying name
    - Can be accessed readily by all players alongside the set of Rules
    - Does not by neccessity depend on resources or tools external to this Nomic and its implementation
    - Is indexed explicitly via this rule (see below)
- `[7.4]` Scoped NRD that is referenced by name in least one other Rule is considered 'in play'. 
- `[7.5]` NRD, scoped or otherwise not currently in play have no effect outside of this rule, even if another rule would allow this. NRD in play may only interact with the game in ways directly specified by rules, even if another rule would allow this. 
- `[7.6]` For resolving conflicts involving NRD, all other rules which refer to the relevant NRD are consulted first in numerical order, then this rule becomes ontested by default if not resolved. (This prevents conflicts which cannot be resolved within the Rules) 
- `[7.7]` If a player wishes to introduce NRD along with a rule which requires it, they must make available all relevant NRD before voting can begin, and how it will be accessed going forwards. If that proposal is adopted, this rule can be modified without rule change to sanction the proposed NRD. This modification is tracked as a rule change to this amendment seperately from the main proposal
- `[7.8]` The following NRD are currently scoped
    - Turn order list
    - Game status
    - Rule actions Archive
    - Definitions
    - The Board
    - Board position
    - Eternity Points

- `[7.9]` Once created, sanctioned NRD may not be modified except by ways permitted by rules which reference them.


## `[8]` Game Resolution
This rule implements the mechanisms by which the game of nomic is restarted upon one of the players winning and a meta-scoring system. The first player to win the game acording to the current victory condition is referred to herein as "the winner".
- `[8.1]` There is a new NRD called "Eternity Points", henceforth EP. EP consists of a rational number assigned to each player. Each player begins with 0 EP. EP may only be interacted with via this rule.
- `[8.2]` When a player becomes the winner, that player must execute the following steps in order
    - `[8.2.1]` The winner's EP increases by 1
    - `[8.2.2]` The current custodian of the *Master Rules Document* will provide an easy to edit copy to the Winner
    - `[8.2.3]` The winner will create a new set of nomic rules satisfying the following conditions
        - They must contain a copy of this entire rule and rule `3.5`
        - One and only one of the rules must specify how a player wins the game (victory condition), no player may begin as the winner, the initial conditions of the player with respect to the victory confition must be equal.
        - The victory condition must be different, in flavour and mechanic, from the initial victory condition in any of the previous nomics
        - The non whitespace character count of the new nomic rules may not change by more than 50% of the non whitespace character count of the current rules
        - There must be an rule mechanism for deciding rules disputes between players (such as a Judge mechanic), such a mechanic must generically and apply equally to all players.
        - The rules should be "playable" and maintain the *spirit of nomic*
- `[8.3]` The current game of nomic ends and a new game of nomic (with all players), played with the winner's new rules, begins. The final state of the *Master Rules Document*,*Amendment Archive* and each player's EP are added as NRD to the new nomic
- `[8.4]` The player(s) with the most EP may use the honorific "Lord/Lady Eternal" in all offical nomic proceedings
- `[8.5]` If multiple players win the game at the same time, the player who completed their last turn the longest time ago is declared the winner, the other players are not.

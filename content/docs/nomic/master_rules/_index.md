---
BookToC: false
bookHidden: true
---

# *Theoretically A Nomic* Master Rules Document
<!-- {{< hint info >}}
**Last Updated:** 04/04/2024
{{< /hint >}} -->

## **`[1]`** Immutable Rules
- `1.01`  All players must always abide by all the rules then in effect, in the form in which they are then in effect. The rules in the Initial Set are in effect whenever a game begins. The Initial Set consists of rules **[1]** (immutable) and **[2]** (mutable). A final set of rules **[3]** set out some basic operations considerations for the game

- `1.02`  Initially, rules in **[1]** are immutable and rules in **[2,3]** are mutable. Rules subsequently enacted or transmuted (ie, changed from immutable to mutable or vice versa) may be immutable or mutable regardless of their numbers, and rules in the Initial Set may be transmuted regardless of their numbers.
- `1.03`  A rule change is any of the following:
the enactment, repeal, or amendment of a mutable rule
the enactment, repeal, or amendment of an amendment
the transmutation of an immutable rule into a mutable rule, or vice versa
(Note: This definition implies that, at least initially, all new rules are mutable. Immutable rules, as long as they are immutable, may not be amended or repealed; mutable rules, as long as they are mutable, may be amended or repealed. No rule is absolutely immune to change.)

- `1.04` 
All rule changes proposed in the proper way shall be voted on. They will be adopted if and only if they receive the required number of votes.
- `1.05` Every player is an eligible voter. Every eligible voter must participate in every vote on rule changes.
- `1.06` Any proposed rule change must be written down before it is voted on. If adopted, it must guide play in the form in which it was voted on.
- `1.07` No rule change may take effect earlier than the moment of the completion of the vote that adopted it, even if its wording explicitly states otherwise. No rule change may have retroactive application.
- `1.08` Each proposed rule change shall be given a rank-order number (ordinal number) for reference. The numbers shall begin with 3.01, and each rule change proposed in the proper way shall receive the next successive integer, whether or not the proposal is adopted.
If a rule is repealed and then re-enacted, it receives the ordinal number of the proposal to re-enact it. If a rule is amended or transmuted, it receives the ordinal number of the proposal to amend or transmute it. If an amendment is amended or repealed, the entire rule of which it is a part receives the ordinal number of the proposal to amend or repeal the amendment.
- `1.09` Rule changes that transmute immutable rules into mutable rules may be adopted if and only if the vote is unanimous among the eligible voters.
- `1.10` Mutable rules that are inconsistent in any way with some immutable rule (except by proposing to transmute it) are wholly void and without effect. They do not implicitly transmute immutable rules into mutable rules and at the same time amend them. Rule changes that transmute immutable rules into mutable rules will be effective if and only if they explicitly state their transmuting effect.
- `1.11` If a rule change as proposed is unclear, ambiguous, paradoxical, or clearly destructive of play, or if it arguable consists of two or more rule changes compounded, or is an amendment that makes no difference, or if it is otherwise of questionable value, then the other players may suggest amendments or argue against the proposal before the vote. A reasonable amount of time must be allowed for this debate. The proponent decides on the final form in which the proposal is to be voted on and decides the time to end debate and vote. The only cure for a bad proposal is prevention: a negative vote.
- `1.12` The state of affairs that constitutes winning may not be changed from achieving N points to any other state of affairs. However, the magnitude of N and the means of earning points may be changed, and rules that establish a winner when play cannot continue may be enacted and (while they are mutable) be amended or repealed.
- `1.13` A player always has the option to forfeit the game rather than continue to play or incur a game penalty. No penalty worse than losing, in the judgement of the player to incur it, may be imposed.
- `1.14` There must always be at least one mutable rule. The adoption of rule changes must never become completely impermissible.
- `1.15`  Rules changes that affect rules needed to allow or apply rule changes are as permissible as other rule changes. Even rule changes that amend or repeal their own authority are permissible. No rule change or type of move is impermissible solely on account of the self-reference or self-application of a rule.
- `1.16`  Whatever is not explicitly prohibited or regulated by a rule is permitted and unregulated, with the sole exception of changing the rules, which is permitted only when a rule or set of rules explicitly or implicitly permits it.

## **`[2]`** Mutable Rules
- `2.01` Players shall take turns in the order (top to bottom) specified by the turn order list. Every turn must be completed in its entirety. Once the active player has finished their turn it becomes the next player's turn. Each player may only be included in the turn order list once. The player currently taking a turn is referred to as the active player. The turn order list will specify which player is the active player. The initial position of a player in the turn order list is at the bottom at time of joining. All players begin with zero points.
- `2.02` 
One turn consists of two parts, in this order:
proposing one rule change and having it voted on
throwing one die once and adding the number of points on its face to one's score
- `2.03` A rule change is adopted if and only if the vote is unanimous among the eligible voters.
- `2.04` If and when rule changes can be adopted without unanimity, the players who vote against winning proposals shall receive 10 points apiece.
- `2.05` An adopted rule change takes full effect at the moment of the completion of the vote that adopted it.
- `2.06` When a proposed rule change is defeated, the player who proposed it loses 10 points.
- `2.07` Each player has exactly one vote.
- `2.08` The winner is the first person to achieve 100 (positive) points.
- `2.09` At no time may there be more than 25 mutable rules.
- `2.10` Players may not conspire or consult on the making of future rule changes unless they are teammates.
- `2.11` If two or more mutable rules conflict with one another, or if two or more immutable rules conflict with one another, then the rule with the lowest ordinal number takes precedence.
If at least one of the rules in conflict explicitly says of itself that it defers to another rule (or type or rule) or takes precedence over another rule (or type of rule), then such provisions shall supersede the numerical method for determining precedence.

If two or more rules claim to take precedence over one another or to defer to one another, then the numerical method must again govern.

- `2.12` 
If players disagree about the legality of a move or the interpretation or application of a rule, then the player preceding the one moving is to be the Judge and to decide the question. Disagreement, for the purposes of this rule, may be created by the insistence of any player. Such a process is called invoking judgement.
When judgement has been invoked, the next player may not begin his or her own turn without the consent of a majority of the other players.

The Judge's judgement may be overruled only by a unanimous vote of the other players, taken before the next turn is begun. If a Judge's judgement is overruled, the player preceding the Judge in the playing order becomes the new Judge for the question, except that no player is to be Judge during his own turn or during the turn of a teammate.

Unless a Judge is overruled, one Judge settles all questions arising from the game until the next turn is begun, including questions as to his or her own legitimacy and jurisdiction as Judge.

New Judges are not bound by the decisions of old Judges. New Judges may, however, settle only those questions on which the players currently disagree and that affect the completion of the turn in which judgement was invoked. All decisions by Judges shall be in accordance with all the rules then in effect; but when the rules are silent, inconsistent, or unclear on the point at issue, then the Judge's only guides shall be common morality, common logic, and the spirit of the game.

- `2.13` 
If the rules are changed so that further play is impossible, or if the legality of a move is impossible to determine with finality, or if by the Judge's best reasoning, not overruled, a move appears equally legal and illegal, then the first player who is unable to complete a turn is the winner.
This rule takes precedence over every other rule determining the winner.

## **`[3]`** Operational Rules

### **`[3.1]`** Game Hosting
- `3.1.01` This game of nomic is called *Theoretically a nomic*.
- `3.1.02` The game is to be hosted on mxwkerr.com
- `3.1.03` The manner of hosting is at the discresion of the webmaster
- `3.1.04` All "publically availiable" game information, such as a player's score will be publically tracked on the game website, along with other relevent gameplay data as the rules evolves

### **`[3.2]`** Game Communication
- `3.2.1` All officially sanctioned and referencable material will be exchanged over a mailing list accessed by all players
- `3.2.3` It is the responsibility of the webmaster to update the game website with the changes implemented in the mailing list

---

- `3.3` Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately roll 1d6 and add the result to their point total. Players who are in a team together are considered teammates.

- `3.4` When this rule is passed for the first time Dylan and epsilon_nought each gain 15 points. This rule has no other effect. Repealing this rule has no effect other than making it's ordinal number available again.

### **`[3.5]`** Non-Rule Data
Players and rules may interact with information external to the ruleset; this rule facilitates the indexing and existence of such information.
- `3.5a` Rule Data is any subset of the Universe currently contained wholly and constructively within the logical span of the current set of Rules
- `3.5b` Non-Rule Data (NRD) is the complement of Rule Data
- `3.5c` NRD is considered 'in scope' or 'scoped' iff it meets the following conditions:
    - Consists of media or information intended for use within this Nomic, and a unique identifying name
    - Can be accessed readily by all players alongside the set of Rules
    - Does not by neccessity depend on resources or tools external to this Nomic and its implementation
    - Is indexed explicitly via this rule (see below)
- `3.5d` Scoped NRD that is referenced by name in least one other Rule is considered 'in play'. 
- `3.5e` NRD, scoped or otherwise not currently in play have no effect outside of this rule, even if another rule would allow this. NRD in play may only interact with the game in ways directly specified by rules, even if another rule would allow this. 
- `3.5f` For resolving conflicts involving NRD, all other rules which refer to the relevant NRD are consulted first in numerical order, then this rule becomes ontested by default if not resolved. (This prevents conflicts which cannot be resolved within the Rules) 
- `3.5g` If a player wishes to introduce NRD along with a rule which requires it, they must make available all relevant NRD before voting can begin, and how it will be accessed going forwards. If that proposal is adopted, this rule can be modified without rule change to sanction the proposed NRD. This modification is tracked as a rule change to this amendment seperately from the main proposal
- `3.5h` The following NRD are currently scoped
- `3.5i` Once created, sanctioned NRD may not be modified except by ways permitted by rules which reference them.
    - Turn order list & Player point totals
    - Game status
    - Rule actions Archive
    - Definitions

### **`[3.6]`** Game Resolution
This rule implements the mechanisms by which the game of nomic is restarted upon one of the players winning and a meta-scoring system. The first player to win the game acording to the current victory condition is referred to herein as "the winner".
- `3.6a` There is a new NRD called "Eternity Points", henceforth EP. EP consists of a rational number assigned to each player. Each player begins with 0 EP. EP may only be interacted with via this rule.
- `3.6b` When a player becomes the winner, that player must execute the following steps in order
    - `3.6b I` The winner's EP increases by 1
    - `3.6b II` The current custodian of the *Master Rules Document* will provide an easy to edit copy to the Winner
    - `3.6b III` The winner will create a new set of nomic rules satisfying the following conditions
        - They must contain a copy of this entire rule and rule `3.5`
        - One and only one of the rules must specify how a player wins the game (victory condition), no player may begin as the winner, the initial conditions of the player with respect to the victory confition must be equal.
        - The victory condition must be different, in flavour and mechanic, from the initial victory condition in any of the previous nomics
        - The non whitespace character count of the new nomic rules may not change by more than 50% of the non whitespace character count of the current rules
        - There must be an rule mechanism for deciding rules disputes between players (such as a Judge mechanic), such a mechanic must generically and apply equally to all players.
        - The rules should be "playable" and maintain the *spirit of nomic*
- `3.6c` The current game of nomic ends and a new game of nomic (with all players), played with the winner's new rules, begins. The final state of the *Master Rules Document*,*Amendment Archive* and each player's EP are added as NRD to the new nomic
- `3.6d` The player(s) with the most EP may use the honorific "Lord/Lady Eternal" in all offical nomic proceedings
- `3.6d` If multiple players win the game at the same time, the player who completed their last turn the longest time ago is declared the winner, the other players are not.
---
BookToC: false
bookHidden: true
title: "Ammendment Archive"
---
# Ammendment Archive


{{< expand "🟩 | 11/05/2024 | A Nomic That Thinks of the Children | epsilon_nought | pass 3v0" "...">}}
## Final Accepted Diff
```diff
@@ -103,4 +103,21 @@ Players and rules may interact with information external to the ruleset; this ru
     - Turn order list & Player point totals
     - Game status
     - Rule actions Archive
-    - Definitions
\ No newline at end of file
+    - Definitions
+
+### **`3.6`** Game Resolution
+This rule implements the mechanisms by which the game of nomic is restarted upon one of the players winning and a meta-scoring system. The first player to win the game acording to the current victory condition is referred to herein as "the winner".
+- `3.6a` There is a new NRD called "Eternity Points", henceforth EP. EP consists of a rational number assigned to each player. Each player begins with 0 EP. EP may only be interacted with via this rule.
+- `3.6b` When a player becomes the winner, that player must execute the following steps in order
+    - `3.6b I` The winner's EP increases by 1
+    - `3.6b II` The current custodian of the *Master Rules Document* will provide an easy to edit copy to the Winner
+    - `3.6b III` The winner will create a new set of nomic rules satisfying the following conditions
+        - They must contain a copy of this entire rule and rule `3.5`
+        - One and only one of the rules must specify how a player wins the game (victory condition), no player may begin as the winner, the initial conditions of the player with respect to the victory confition must be equal.
+        - The victory condition must be different, in flavour and mechanic, from the initial victory condition in any of the previous nomics
+        - The non whitespace character count of the new nomic rules may not change by more than 50% of the non whitespace character count of the current rules
+        - There must be an rule mechanism for deciding rules disputes between players (such as a Judge mechanic), such a mechanic must generically and apply equally to all players.
+        - The rules should be "playable" and maintain the *spirit of nomic*
+    - `3.6c` The current game of nomic ends and a new game of nomic (with all players), played with the winner's new rules, begins. The final state of the *Master Rules Document*,*Amendment Archive* and each player's EP are added as NRD to the new nomic
+- `3.6d` The player(s) with the most EP may use the honorific "Lord/Lady Eternal" in all offical nomic proceedings
+- `3.6d` If multiple players win the game at the same time, the player who completed their last turn the longest time ago is declared the winner, the other players are not.
\ No newline at end of file
```

## Debate

**Dylan**

I am very much in favour of this, although could maybe use a clause about if more than one person somehow wins at the same time
Which I think is currently possible

---

**epsilon_nought**

So the first thought I had was to use turn order as the tiebreaker, with active player having the highest priority then going in turn order, but winning not in your own turn feels more impressive, so I would be tempted for something like

If multiple players win the game at the same time, the player who completed their last turn the longest time ago is declared the winner, the other players are not.

---

**Dylan**

That's a good idea

---

**epsilon_nought**

Ok, I will open final voting some time this evening if there are no more proposed changes

---

**epsilon_nought**

Voting on accepting the proposal is now oper 
Accept the proposal?
Select one answer
79m left

---

**Dylan**

Are you adding the tiebreaker?
Oh nevermind I just clicked on the old version

---

**Dylan** 

Oh nice it passed

---

**epsilon_nought** 

The proposal passes

---

*epsilon_nought gains 4 points*


{{< /expand >}}

{{< expand "🟩 | 01/05/2024 | Non-Rule Data | Dylan | pass 3v0" "..." >}}
## Final Accepted Diff
```diff
--- a/content/docs/nomic/master_rules/_index.md
+++ b/content/docs/nomic/master_rules/_index.md
@@ -83,4 +83,24 @@ This rule takes precedence over every other rule determining the winner.
 
 - `3.3` Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately roll 1d6 and add the result to their point total. Players who are in a team together are considered teammates.
 
-- `3.4` When this rule is passed for the first time Dylan and epsilon_nought each gain 15 points. This rule has no other effect. Repealing this rule has no effect other than making it's ordinal number available again.  
\ No newline at end of file
+- `3.4` When this rule is passed for the first time Dylan and epsilon_nought each gain 15 points. This rule has no other effect. Repealing this rule has no effect other than making it's ordinal number available again.  
+
+### **`[3.5]`** Non-Rule Data
+Players and rules may interact with information external to the ruleset; this rule facilitates the indexing and existence of such information.
+- `3.5a` Rule Data is any subset of the Universe currently contained wholly and constructively within the logical span of the current set of Rules
+- `3.5b` Non-Rule Data (NRD) is the complement of Rule Data
+- `3.5c` NRD is considered 'in scope' or 'scoped' iff it meets the following conditions:
+    - Consists of media or information intended for use within this Nomic, and a unique identifying name
+    - Can be accessed readily by all players alongside the set of Rules
+    - Does not by neccessity depend on resources or tools external to this Nomic and its implementation
+    - Is indexed explicitly via this rule (see below)
+- `3.5d` Scoped NRD that is referenced by name in least one other Rule is considered 'in play'. 
+- `3.5e` NRD, scoped or otherwise not currently in play have no effect outside of this rule, even if another rule would allow this. NRD in play may only interact with the game in ways directly specified by rules, even if another rule would allow this. 
+- `3.5f` For resolving conflicts involving NRD, all other rules which refer to the relevant NRD are consulted first in numerical order, then this rule becomes contested by default if not resolved. (This prevents conflicts which cannot be resolved within the Rules) 
+- `3.5g` If a player wishes to introduce NRD along with a rule which requires it, they must make available all relevant NRD before voting can begin, and how it will be accessed going forwards. If that proposal is adopted, this rule can be modified without rule change to sanction the proposed NRD. This modification is tracked as a rule change to this amendment seperately from the main proposal
+- `3.5h` The following NRD are currently scoped
+- `3.5i` Once created, sanctioned NRD may not be modified except by ways permitted by rules which reference them.
+    - Turn order list & Player point totals
+    - Game status
+    - Rule actions Archive
+    - Definitions
```

## Debate
**Dylan** 

Unfortunately we still don't have subrules, so the indexing here is purely for visual separation
Proposal

3.2.1 -> 3.5 Non-Rule Data
Players and rules may interact with information external to the ruleset; this rule facilitates the indexing and existence of such information.

3.5a Rule Data is any subset of the Universe currently contained wholly and constructively within the logical span of the current set of Rules

3.5b Non-Rule Data (NRD) is the complement of Rule Data

3.5c NRD is considered 'Sanctioned' iff it meets the following conditions:

Consists of media or information intended for use within this Nomic, and a unique identifying name

Can be accessed readily by all players alongside the set of Rules

Does not by neccessity depend on resources or tools external to this Nomic and its implementation

Is indexed explicitly via this rule (see below)

3.5d Sanctioned NRD that is referenced by name in least one other Rule is considered 'in play'. 

3.5e NRD, sanctioned or otherwise not currently in play have no effect outside of this rule, even if another rule would allow this. NRD in play may only interact with the game in ways directly specified by rules, even if another rule would allow this. 

3.5f For resolving conflicts involving NRD, all other rules which refer to the relevant NRD are consulted first in numerical order, then this rule becomes contested by default if not resolved. (This prevents conflicts which cannot be resolved within the Rules)

3.5g If a player wishes to introduce NRD along with a rule which requires it, they must make available all relevant NRD before voting can begin, and how it will be accessed going forwards. If that proposal is adopted, this rule can be modified without rule change to sanction the proposed NRD.

3.5h The following NRD are currently Sanctioned

Turn order list & Player point totals

Game status

Rule actions Archive

Definitions

Then I would also like to make available the NRD titled Definitions, containing one line

a die is one fair d6.

---

**Dylan**

Oh I forgot a line
Proposed amendment:
> 3.5i Once created, sanctioned NRD may not be modified except by ways permitted by rules which reference them. 

---

**epsilon_nought**

I would prefer to have 3.5g labelled as a specific new rules-change action, perhaps we can call it substantiation, for ease of reference and for making triggers that reference the creation of new NRD. My amendment would be something like

> 3.5g New Game action (substantiation) As a rule action, a player may introduce new NRD. The player must make all relevant NRD before voting can begin, and how it will be accessed going forwards. The substantiated new NRD is subject to the same timing restriction as a new rule under 1.07.  The new NRD must also be tracked under 3.5h

Furthermore, I suggest a change of wording, sanctioned -> admissible, since I think sanctioned is likely to be used in a different context in the future.

These are mostly just flavour changes though.

---

**Dylan**

So I could make it a game action, but the idea is you propose a rule and all the relevant NRD all at the same time like we have been doing anyway
Like the turn list
Otherwise it would take two full turns to implement something like that

---

**epsilon_nought**

I intended to keep the whole things as a single action, what makes it sound like two seperate actions? 

---

**Dylan**

It sounds like you're using your turn to take this action instead of proposing a rule change

---

**epsilon_nought**

I intended that this action should be one of the "rule change actions" available to the player
How about...

> 3.5g Introducing new NRD is called substantiation and is counted as a rules change under 2.02. To substantiate , a player introduces new NRD along with a rule using the NRD. The player must make all relevant NRD available before voting can begin, and describe how it will be accessed going forwards. Any new NRD is automatically amended to 3.5h.

---

**Dylan**

Ok that makes a lot more sense, it is a separate rules change for anything that cares about that but happens simultaneously with another rule change
Functionally the rules change can be tracked as the amendment to 3.5

---

**epsilon_nought**

Yes, a "rules change" I think can include the changing of multiple rules, it is just that the "rule change" actions currently supported are the transmutation, repeal or enactment of a single rule.
I think that a definition of player actions including "rules change" actions and other actions like team forming might also be useful to add to the definitions doc, to keep track of the different gameplay actions, at least until they are codified in the rules.
I also don't like substantiation as a name anymore, but I still think something different form the language of "sanction"

---

**Dylan**

I'll let someone else do that in the future
I was thinking "in scope" or just "scoped"

---

**epsilon_nought**

So ideally there is a name for the action of creating NRD and then an adjective for them.

---

**Dylan**

Ok, I'll revise it sometime this evening

---

**epsilon_nought**

I'll work on adding a definitions page to the website, assuming SalmonCannon lets this pass

---

**SalmonCannon**

I sanction this substantiation

---

**epsilon_nought**

Misuse of the sanctions is punishable by further sanctions

---

**Dylan** 

Final form of the amendment

3.2.1 -> 3.5 Non-Rule Data

Players and rules may interact with information external to the ruleset; this rule facilitates the indexing and existence of such information.

3.5a Rule Data is any subset of the Universe currently contained wholly and constructively within the logical span of the current set of Rules

3.5b Non-Rule Data (NRD) is the complement of Rule Data

3.5c NRD is considered 'in scope' or 'scoped' iff it meets the following conditions:

Consists of media or information intended for use within this Nomic, and a unique identifying name

Can be accessed readily by all players alongside the set of Rules

Does not by neccessity depend on resources or tools external to this Nomic and its implementation

Is indexed explicitly via this rule (see below)

3.5d Scoped NRD that is referenced by name in least one other Rule is considered 'in play'. 

3.5e NRD, scoped or otherwise not currently in play have no effect outside of this rule, even if another rule would allow this. NRD in play may only interact with the game in ways directly specified by rules, even if another rule would allow this. 

3.5f For resolving conflicts involving NRD, all other rules which refer to the relevant NRD are consulted first in numerical order, then this rule becomes contested by default if not resolved. (This prevents conflicts which cannot be resolved within the Rules) 

3.5g If a player wishes to introduce NRD along with a rule which requires it, they must make available all relevant NRD before voting can begin, and how it will be accessed going forwards. If that proposal is adopted, this rule can be modified without rule change to sanction the proposed NRD. This modification is tracked as a rule change to this amendment seperately from the main proposal

3.5h The following NRD are currently scoped

3.5i Once created, sanctioned NRD may not be modified except by ways permitted by rules which reference them.

Turn order list & Player point totals

Game status

Rule actions Archive

Definitions

Then I would also like to make available the NRD titled Definitions, containing one line
> a die is one fair d6.
Apparently discord has a message limit

--

*Proposal passes 3v0*
*Dyland gains

---

**epsilon_nought**

I love democracy

--



{{< /expand >}}


{{< expand "🟩 | 28/04/2024 | Salmoncannon is sorry | Salmoncannon | pass 3v0" "..." >}}
## Final Accepted Diff
```diff
--- a/content/docs/nomic/master_rules/_index.md
+++ b/content/docs/nomic/master_rules/_index.md
@@ -81,4 +81,6 @@ This rule takes precedence over every other rule determining the winner.
 
 ---
 
-`3.3` Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately roll 1d6 and add the result to their point total. Players who are in a team together are considered teammates.
\ No newline at end of file
+`3.3` Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately roll 1d6 and add the result to their point total. Players who are in a team together are considered teammates.
+
+- `3.4` When this rule is passed for the first time Dylan and epsilon_nought each gain 15 points. This rule has no other effect. Repealing this rule has no effect other than making it's ordinal number available again.  
\ No newline at end of file
```
## Proposal

> When this rule is passed for the first time Dylan and epsilon_nought each gain 15 points. This rule has no other effect. Repealing this rule has no effect other than making it's ordinal number available again. 

## Debate

**epsilon_nought** 

I think that something more interesting is in order, whack amendment incoming.

> There now exist two new game resources, matter and antimatter. salmoncannon gains 1 matter, Dylan and epsilon_nought gain 1 anti-matter each. Unless expanded upon by a future rule these resources remain inert (i.e. wit no mechanical interactions)

And

> The Game Status on the game website will be changed from equlibrium to "SalmonCannon is very sorry".

What we will do with this resource...is anyone's guess.

---

**SalmonCannon** 

Are these amendments intended to add to or replace the rule I proposed?

---

**Dylan** 

I was gonna propose that everyone gets 5 dollars but that is also funny
Although you should probably do that on your turn, as I think trying to totally replace one proposal with another could be considered collusion

---

**epsilon_nought** 

Well if SalmonCannon is not sorry enough to give me some damn antimatter then I suppose I can live with that.

---

**Dylan** 

He can do it if he wants, but only if you guys form a team first, as the outcome would essentially be the purchase of your vote in exchange for allowing you to take an extra game action

---

**SalmonCannon** 

If he proposes an amendment to my rule and I accept it how is that collusion?

---

**Dylan** 

For instance, if I proposed an amendment that entirely replaces the text of someone else's rule with my own, then that person would effectively have the confidence of my vote  whilst I got to propose an additional rule per turn cycle
I think it would be a bad precedent at the least

---

**SalmonCannon** 

Is it meant to replace or add to the text of the rule?

---

**epsilon_nought** 

To add

---

**Dylan** 

That might be a problem with "this rule has no other effect"
I would say this amendment is outside the scope of 1.11, but I suppose that's an ambiguous line

---

**epsilon_nought** 

Well until the loophole is closed I have a nomic given right to exploit it. The only way to close it would be to change the rule via vote or invoke judgment on 1.11

---

**Dylan** 

True
Would you agree that it is collusion to ask a player to propose a certain ruling on their turn however
In advance

---

**SalmonCannon** 

In advance I think yes certainly

---

**epsilon_nought** 

Players can agree about the general principals of the direction of the game. If I say that 1.08 is problematic and suggest a player should amend it, that should not be counted as collision.

---

**SalmonCannon** 

Especially if not all players are involved in that conversation

---

**epsilon_nought** 

I think the proposal would have to be specific, and then it would count.
Shall we begin the Nomic Case Law document and add this ruling to it?

---

**Dylan** 

Sure, it's probably important to know what counts as collusion in future

---

**epsilon_nought** 

I will consult relevant existing law

https://www.law.cornell.edu/uscode/text/18/201

And come up with a ruling for the case law book. We can vote on and amend  it separately from the game, if everyone is down for that.

---

**Dylan** 

So I was planning on introducing a way to define specific words within the game if you wanted to do it that way

---

**epsilon_nought** 

On your turn?

---

**Dylan** 

Yes

---

**epsilon_nought** 

Ok, I will wait to see what you come up with and see how I can build on it.
Until then I maintain my proposed additions that @
---

**SalmonCannon** is free to accept or reject of his own free will.

---

**Dylan** 

I will say I probably wouldn't support this current proposal if amended in the way you have suggested, on the grounds that it sets the precedent for potential buying and selling of votes as well as introducing a resource system simultaneously.
There is probably a version of it that is fine however

---

**epsilon_nought** 

I will support the amended or original version of the rule.

---

**SalmonCannon** 

Ok, we can move to voting. The final version of the proposed rule is:

> `3.4` *SalmonCannon is very sorry*. When this rule is passed for the first time Dylan and epsilon_nought each gain 15 points. This rule has no other effect. Repealing this rule has no effect other than making its ordinal number available again.

---

**SalmonCannon** 

The proposal is adopted

---

**epsilon_nought** 

I love democracy

---
*SalmonCannon* rolld a d120 for 27 points

---

**epsilon_nought** 

I don't think you roll a d120 do you?
It is 1d6

---

**SalmonCannon** 

`2.02` doesn't specify
It says you throw a die

---

**epsilon_nought** 

Grumple grumple
Hmm
I am tempted to invoke Judgment....
Dylan what are your feelings?

---

**Dylan** 

Mmm I think he got you there
I was genuinely about to add that we change it from 15 to the number of points rolled plus n

---

**epsilon_nought** 

Well even if I invoke judgment, I think I am in the minority and so It would be decided against me.
You are very sporting though, not immediately throwing a d100000

---

**Dylan** 

True
I've also realised that at no point does it say winning ends the game
Or actually does anything at all

---

**SalmonCannon** 

I considered that but 120 sides is apparently the highest number that can be mathematically fair as a physical die in 3d space

---

**epsilon_nought** 

Fair enough
I will not contest this

---

**SalmonCannon** 

I expected to roll better 😆

{{< /expand >}}



{{< expand "🟥 | 28/04/2024 | Transmutation of 1.08 & The Ordinal controversy | epsilon_nought | fail 2v1" "..." >}}
## Proposal
> rule 1.08 should be made mutable.

## Debate

**SalmonCannon** — 23/04/2024 

I'd go along with that

---

**epsilon_nought** — 23/04/2024 

I still want to see the subgame happen, but it can be used to change another one of the rules. 

---

**Dylan** — 25/04/2024 

My only objection is that if this counts as a rule change, it needs a number by 1.08, and if it is not a rule change then it is not clear if we are allowed to suggest amendments or have any debate via 1.11. 
Thus my comment is in superposition until further clarification

---

**SalmonCannon** — 26/04/2024 

1.03 says that the transmutation of a immutable rule into a mutable rule is a rule change, so we do need a number and we are allowed to debate

---

**epsilon_nought** 

Can I propose the name:1.08?

---

**Dylan** 

That's not available

---

**epsilon_nought** 

Or, should we think of 1.08 as providing each rule a sort of universal index, i.e. when we passed 3.3 we should have given it a new index, strictly speaking 3.01, if it were updated later it would still be called 3.3 but have an updated "index".  To clarify this we could introduce a new rule presentation convention, where rules are listed with an identifier mainly used for reference as they currently are, along with an ordinal the implements the mechanics of 1.08 and 2.11

So each rule would be written:

1.x.y.z \[3.XY\] Insert Rule Text Here

And we will likely want to reform the ordinal numbering so it does not cause any confusion with the rule references 

---

**SalmonCannon** 

1.08 is very clear: each rule change gets the next available ordinal numbrr

---

**epsilon_nought** 

So the change would take `1.08->3.4` then I guess

---

**SalmonCannon** 

Transmuting a rule is a rule change, and when a rule is transmuted it receives the ordinal number of the rule transmuting it
I think `3.4` was forming a team
So this is `3.5`

---

**epsilon_nought** 

`3.3` was teams
I refer you to the Master rules document 
So the proposal as stands is

> 1.08 -> 3.4 (Rule is moved out of 1.X and so becomes mutable)

If there are no amendments to this then I will open final voting in about an hour

---

**Dylan** 

So I could be wrong, but at no point does making a rule mutable seem to change the number
This rule change to make 1.08 mutable is numbered 3.4 or 3.5 whichever, but 1.08 would still be 1.08
If it was an amendment to a mutable rule then that would change the number

---

**SalmonCannon** 

`1.08` says:

> ...If a rule is amended or transmuted, it receives the ordinal number of the proposal to amend or transmute it...

---

**epsilon_nought** 

But is that the same number that describes each rule, or does each rule have two numbers a "name" and the ordinal assigned by 1.08

---

**SalmonCannon** 

There is only one number for each rule: the ordinal number
Otherwise the rules in the Initial Set wouldn't have an ordinal number and their priority wouldn't be defined

---

**epsilon_nought** 

Ok, final vote, with the clarification above

Given **SalmonCannon** 's previous message, I open voting on

> 1.08 -> 3.4 (Rule is moved out of 1.X and so becomes mutable)

As the proponent of the transmutation, I will accept no further amendments to the proposal, as Is my right.

---

**Dylan** 

So if it ever matters, this proposal itself "gets" the number 3.4, then the mutated 1.08 receives that number if it gets enacted.
It is unclear if that actually means anything or if rule changes actually exist outside the bounds of voting but I think it is very funny to have on the record

---

**epsilon_nought** 

And so on the record it shall be

---

**epsilon_nought** 

The  motion is defeated

*epsilon_nought looses 10 points*
{{< /expand >}}


{{< expand "🟥 | 20/04/2024 | Enactment of 3.5 & The 1st Subgame | Dylan | fail 2v1" "..." >}}
## Proposal

3.5.1 When this rule is passed for the first time and points have been rolled, immediately begin a subgame of Nomic.

3.5.2 The ruleset for this subgame is identical to the ruleset at the time this rule passes, except for this rule (rule 3.5 and subrules therein) and immutable rule 1.08, which is to be replaced with '1
umbers and Formatting' as described below.

3.5.3 All non-rule game data, including point totals, turn order and teams are copied to the subgame on creation, and the next player in turn order becomes the active player.

3.5.4 The current game (referred to as the supergame) is considered to be in suspension; no game actions or rule changes may occur until the subgame has ended. No rule or action from within a subgame may reference or alter rules in the supergame. 

3.5.5 When the subgame ends, the turn list of that game is duplicated to the supergame. If the subgame had a winner, that player's point total in the supergame is then immediately set to 100.

3.5.6 All details regarding game hosting subject to 3.1 are to be kept identical, up to the discression of the webmaster. Rules which are only part of a supergame may be archived and removed from the main rule list for clarity.

3.5.7 Supplementary: Proposed rule 1.8. 

'1.8: Numbers and Formatting' 

1.8.1 Rules are indexed by a tuple of integers, formatted as x.x.x and so on.

1.8.2 The first index specifies the category

1.8.3 The second index gives the ordinal number of the rule. Rules with two indices are also called "Top level rules".

1.8.4 Rules may have any number of subrules, which have no mechanical effect other than visual clarity. When enacting or modifying a rule, a player may add or change any number of subrules under one top level rule at once.

1.8.5 Rules and subrules may optionally have a name. The name of a rule has no effect, however it is a suitable alias for the purpose of cross reference.

## Debate

**epsilon_nought** 

We are not worthy of such greatness

---

**SalmonCannon** 

Just beautiful

---

**epsilon_nought** 

Proposed clarifications

1.8.4 Rules may have any number of subrules, which have no mechanical effect other than visual clarity. When enacting or modifying a rule, a player may add or change any number of subrules under one top level rule at once. Thus rule X.Y.AB is considered as the "same rule" as X.Y . Subrules are considered distinct only for the purposes of applying 2.11 when two subrules of the same rule conflict

1.8.5 Categories, Rules and subrules may optionally have a name. The name of a rule has no effect, however it is a suitable alias for the purpose of cross reference.


Requested Amendments

1.8.6 When a new rule is proposed, it must be specified which category or top-level rule it falls under. A single new category may be invented to this effect.  If accepted the rule is endowed with the next ordinal number in the relevant listing (beginning at 1)
 

---

**Dylan** 

Will slightly tweak the wording

---

**Dylan** 

Proposed clarifications

1.8.4 Rules may have any number of subrules, which have no mechanical effect other than visual clarity. When enacting or modifying a rule, a player may add or change any number of subrules under one top level rule at once. Subrules are not considered distinct rules, and reference to a subrule should be taken an alias for the entire top level rule. If two or more subrules under one top level rule would conflict, that entire rule is considered invalid.

1.8.5 Categories, Rules and subrules may optionally have a name. The name of a rule has no effect, however it is a suitable alias for the purpose of cross reference.


Requested Amendments

1.8.6 When a new rule is proposed, it must be specified which category it falls under. Unless specified by another rule, new mutable rules must be proposed under category 2, with the next available index greater than 1.
1.8.7 When a rule changes category, the index changes to reflect this, and it is given the next available index in that category. References to that rule by number may be updated to reflect this new index.

---

**SalmonCannon** 

So amending a rule no longer changes its index?

---

**epsilon_nought** 

Yes, I think I prefer it that way, so cross referencing is easier to maintain

---

**Dylan** 

You could explicitly change the index of an existing rule if you modify it

---

**epsilon_nought** 

If the changing of a rule Index is not forbidden, it is allowed. Should probably add a provision that says all rules should have all but the last of their "ordinals" fixed by their position, so rule 1.2.3 does not end up as a sub rule of 3.4 .ect

---

**Dylan** 

I think it's fine as is, being a subrule is based on inclusion in the text of the rule rather than the number
Only the first two indices have real meaning
Technically you can label subrules however you want

---

**Dylan** 

So unless anyone has pressing concerns or other game breaking bugs in immutable rules I'm down to put this to a vote

---

**epsilon_nought** 

I am happy to move to the final vote

---

**SalmonCannon** 

Same

---

**Dylan** 

I'd post it all again but apparently it's too long for discord
Adopt this rule?
Select one answer
22h left

---

**epsilon_nought** 

A shock outcome!

---

**Dylan** 

Damn fair enough

---

**epsilon_nought** 

I had already rejiged the website and all
Ah well...

---

**SalmonCannon** 

It still gets adopted right?
Votes don't have to be unanimous

---

**epsilon_nought** 

2.03 A rule change is adopted if and only if the vote is unanimous among the eligible voters.

---

**SalmonCannon** 

Oh shit ok
Why does rule 2.04 exist then?

---

**epsilon_nought** 

If and when rule changes can be adopted without unanimity

---

**SalmonCannon** 

I was just trying to get a free 10 points

---

**epsilon_nought** 

If a change in the future would allow
Now ye have paid the price

---

**Dylan** 

Dylan will remember this

---

*Dylan looses 10 points*

{{< /expand >}}


{{< expand "🟩 | 18/04/2024 | Enactment of 3.3 | SalmonCannon | pass 3v0" "..." >}}
```diff
--- a/content/docs/nomic/master_rules/_index.md
+++ b/content/docs/nomic/master_rules/_index.md
@@ -77,4 +77,7 @@ This rule takes precedence over every other rule determining the winner.
 
 ### **`[3.2]`** Game Communication
 - `3.2.1` All officially sanctioned and referencable material will be exchanged over a mailing list accessed by all players
-- `3.2.3` It is the responsibility of the webmaster to update the game website with the changes implemented in the mailing list
\ No newline at end of file
+- `3.2.3` It is the responsibility of the webmaster to update the game website with the changes implemented in the mailing list
+
+
+- `3.3` Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately roll 1d6 and add the result to their point total. Players who are in a team together are considered teammates.
```
## Debate

**SalmonCannon** 

> Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately roll 1d6 and add the result to their point total. Players who are in a team together are considered teammates.

---

**epsilon_nought** 

Pursuant to 1.16, I move to have the above rule entered into a new super-set of rules, superseding 1.08 in this instance

Proposed Clarificati
mbed the proposed rule in the new section Headed "4 Game actions / 4.1  Forming A Team/ 4.1.1 (The Proposed Rule)" 

---

**SalmonCannon** 

I would argue that the ordinal numbering of new rules is "explicitly regulated" by 1.08

---

**epsilon_nought** 

Hmm. 1.08 Does not explicitly state that players may not overrule this, however I will yield to the precedence of 1.08 over 1.16 as described by 2.11

---

**Dylan** 

It appears that 1.08 is the only mechanism by which a rule obtains its number, so attempting to overrule this by giving a different number in the proposal of a mutable rule would violate 1.10

---

**epsilon_nought** 

Since rules 3.1 and 3.2 already exist, should 1.08 be interpreted as giving the next numerical identifier, i.e. 3.3?

---

**Dylan** 

Sure

---

**SalmonCannon** 

The alternative is to say that 3.01 is different to 3.1 which would be interesting...

---

**epsilon_nought** 

Is 01 not the same ordinal as 1?

---

**SalmonCannon** 

Yeah I guess it is, this is then 3.3

---

**Dylan** 

No the previous rule would have been 3.3, this is 3.4

---

**SalmonCannon** 

Oh yeah

---

**Dylan** 

Anyway I have no amendments to the proposed rule

---

**epsilon_nought** 

Same

---

**SalmonCannon** 

Wait I actually do want to amend it
I think 1d6 is not enough of a compensation for those not included, given how strong collusion could be - especially in a 3 player
Proposed amendment:

> Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately gain 10 points. Players who are in a team together are considered teammates.

---

**Dylan** 

This is also fine

---

**epsilon_nought** 

Yes

---

**SalmonCannon** 

Ok, @everyone I open final voting on the proposal, the final form of which is

`3.4`
> Any number of players may at any time form a team. A team is formed when one player declares their intention to form a team with any number of other players,  and then all of these other players accept. These declarations and acceptance statements must be communicated to all players at the moment they are made. When a team is formed all players not on that team immediately gain 10 points. Players who are in a team together are considered teammates.

---

*SalmonCannon gains 4 points*

{{< /expand >}}


{{< expand "🟩 | 18/04/2024 | Ammendment of 2.01 | epsilon_nought | pass 3v0" "..." >}}
## Final Accepted Diff
```diff
--- a/content/docs/nomic/master_rules/_index.md
+++ b/content/docs/nomic/master_rules/_index.md
@@ -35,7 +35,7 @@ If a rule is repealed and then re-enacted, it receives the ordinal number of the
 - `1.16`  Whatever is not explicitly prohibited or regulated by a rule is permitted and unregulated, with the sole exception of changing the rules, which is permitted only when a rule or set of rules explicitly or implicitly permits it.
 
 ## **`[2]`** Mutable Rules
-- `2.01` Players shall alternate in clockwise order, taking one whole turn apiece. Turns may not be skipped or passed, and parts of turns may not be omitted. All players begin with zero points.
+- `2.01` Players shall take turns in the order (top to bottom) specified by the turn order list. Every turn must be completed in its entirety. Once the active player has finished their turn it becomes the next player's turn. Each player may only be included in the turn order list once. The player currently taking a turn is referred to as the active player. The turn order list will specify which player is the active player. The initial position of a player in the turn order list is at the bottom at time of joining. All players begin with zero points.
 - `2.02` 
 One turn consists of two parts, in this order:
 proposing one rule change and having it voted on
```

## Debate
**epsilon_nought**

Proposed changing of the text of 2.01 to
> Players shall take turns in the order (top to bottom) specified by the turn order list.  Turns may not be skipped or passed, and parts of turns may not be omitted. The initial position of a player in the turn order list is at the bottom at time of joining. All players begin with zero points.

---

**SalmonCannon**

Proposed amendment:
> Players shall take turns in the order (top to bottom) specified by the turn order list. Turns may not be skipped or passed, and parts of turns may not be omitted. Each player may only be included in the turn order list once. The player currently taking a turn is referred to as the active player. The turn order list will specify which player is the active player. The initial position of a player in the turn order list is at the bottom at time of joining. All players begin with zero points.

---

**epsilon_nought**

I accept @SalmonCannon 's proposed variation on my amendment. By `1.11` as the proponent I set the length of the debate period. In 1 hr I will end the debate and will open voting on the proposal

---

**epsilon_nought**

Proposed amendment:
> Once a player has resolved a full game action on their turn, it becomes the next players turn.

---

**SalmonCannon**

I accept this very sensible and necessary amendment
Wait I don't know if "game action" is defined anywhere
Proposed amendment to the proposed amendment:
> Once the active player has finished their turn it becomes the next player's turn

---

**epsilon_nought**

I open final voting on the proposal, the final form of which is

`2.01`
> Players shall take turns in the order (top to bottom) specified by the turn order list. Every turn must be completed in its entirety. Once the active player has finished their turn it becomes the next player's turn. Each player may only be included in the turn order list once. The player currently taking a turn is referred to as the active player. The turn order list will specify which player is the active player. The initial position of a player in the turn order list is at the bottom at time of joining. All players begin with zero points.

---
Vote Passes 3 against 0

epsilon_nought gains 2 points

{{< /expand >}}
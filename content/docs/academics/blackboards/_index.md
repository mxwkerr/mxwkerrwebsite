---
title: "Blackboards"
weight: 3
BookToC: false
---
# Blackboards


<center><em>Febuary 1st, 2024</em></center>

![Febuary 1st](boardimages/2024/2/1.jpg)

<center> Rough sketching of ideas with visitor </center>

### Blackboard Archive
{{< expand "2024" "..." >}}
[`January 16th`](boardimages/2024/1/16.jpg) Trying to understand what the square <em>root of a differential form</em> is and how it operates. These objects are important for the <a href = "https://ncatlab.org/nlab/show/geometric+quantization#Polarizations">geometric quantisation</a> of so-called <em>real polarisation</em> 

[`January 30th`](boardimages/2024/1/30.jpg)
The 1/2-form corrected geometric quantisation of the position operator (spoilers - which is just multiplication). This is the first time I have written out this derivation and understood every part of it.

[`January 31st`](boardimages/2024/1/31.jpg)
Doing some conversions to complex coordiantes to look at the 1/2-form corrected Kahler quantisation of the harmonic oscillator

[Febuary 1st](boardimages/2024/2/1.jpg)
Rough sketching of ideas with visitor

{{< /expand >}}
{{< expand "2023" "..." >}}
[`December 18th`](boardimages/2023/12/18.jpg) Understanding the argument that <em>quantisation commutes with reduction</em> as described in Guillemin and Sternberg's 1982 paper  <a href = "https://doi.org/10.1007/BF01398934"><em>Geometric quantization and multiplicities of group representations</em></a>

[`November 26th`](boardimages/2023/11/26.jpg) Trying to understand the gauge invariance of the covaraint derivative  for the <a href="https://ncatlab.org/nlab/show/prequantum+line+bundle"><em>prequantum line bundle</em></a> 

[`November 23th`](boardimages/2023/11/23.jpg) Blackboard discussion of some general ideas 

{{< /expand >}}
---
title: "Seminars"
weight: 3
---

# Seminars

## 2024
{{< details title="`Friday, 16th August` - *Geometric Quantistaion and Optics*, University of Glasgow Quantum Theory Group" open=false >}}
[`Seminar Notes`](Glasgow_QT_Seminar_2024.pdf)
{{< /details >}}


{{< details title="`Wednesday, 13th March` - *Towards Differential Geometry: Vector Fields & Curvature*, EPSRC CDT In Topological Design" open=false >}}
[`Seminar Notes`](Kerr_CDT_Seminar.pdf)
{{< /details >}}

{{< details title="`Thursday, 25th January` - *Introduction to the Geometry of Quantisation*, University of Birmingham Theory Group" open=false >}}
[`Seminar Notes`](BHAM2024_Geometry_of_Quantisation.pdf)
### Abstract
Quantisation, as formalised by Dirac, is the process of lifting a classical mechanical theory to a quantum mechanical theory in symmetry respecting fashion. An example of such a procedure is the canonical quantisation of $q$ and $p$ known to all physicists. It is well known that a complete quantisation in the sense of Dirac is fundamentally limited by the Groenewold-van Hove theorem, but less known to physicists are the influences that geometry and topology can have on quantisation. On topologically trivial and flat phase spaces (such as those inhabited by particles in mathbb{R}^n subject to a potential) canonical quantisation can be applied without fear. In this seminar we will introduce the formalism of Geometric Quantisation, which constructs and extends canonical quantisation to curved and topologically interesting phase spaces (i.e. general symplectic manifolds). Such spaces arise both physically (particle moving on a not simply-connected or curved surface) and more abstractly (reduced phase spaces of integrable systems).

Our goal is to give a broad overview of the ideas involved in constructing a general theory of quantisation, motivate why such a theory is necessary in the first place and explore to what extent geometric quantisation succeeds at providing a physically productive solution. We will also argue that geometric quantisation introduces interesting questions regarding the physical ontology of the (arbitrarily chosen) mathematical structures it introduces. Finally, geometric quantisation furnishes sufficient mathematical structure to allow many intriguing connections to other fields of physics and mathematics to be observed and exploited. These include links to representation theory, index theorems and topological field theories, all of which we will visit briefly along the way.

Note: This will primarily be a blackboard talk. Some knowledge of differential geometry will be beneficial but not necessary, a quick revision of the basics of fibre bundles will be included.

{{< /details >}}

{{< details title="`Wednesday, 14th Febuary` - *Semiclassical Spirograph - Semiclassical Methods for Caustic Structured Light*, EPSRC CDT In Topological Design" open=false >}}
NONE
{{< /details >}}



---

## 2023
{{< details title="`Thursday, 12th October` - *Semiclassical Spirograph - Semiclassical Methods for Caustic Structured Light*, University of Birmingham Theory Group" open=false >}}
[`Slides`](Bham_Aston_Seminar_2023.pdf)
### Abstract
Structured Gaussian modes, special function solutions to the paraxial wave equation with a Gaussian envelope, are a central tool of modern optical theory and experiment. Despite having a prescribed structure with an abundance of identities to aid computation, efficiently propagating these structured light modes is a hard analytical and numerical problem. In this talk I will discuss a ray-optical approach to constructing semiclassical approximations of these modes by quantizing the space of rays, enabling efficient calculation with Gaussian beamlets and in doing so expose all manner of wonderful geometry underlying the symmetry and quantization of the problem. I will also show how this structure suggests new natural approaches to mode design in “real” and “operator” space.

{{< /details >}}

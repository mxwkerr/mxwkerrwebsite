---
title: "TopPhoto23"
bookHidden: true
weight: 2
---
# TopPhoto23
*Topological Photonics 2023, Madrid, Spain*
*31 May – 2 June 2023*
{{< columns >}}
### [`Conference Website`](https://eventos.uam.es/88595/detail/topological-photonics-2023.html)
<---> 
### [`My Poster`](Kerr_TopologicalPhotonics_2023_Final.pdf)
<---> 
### `Conference Photo`
{{< /columns >}}

# And here we begin again



---
title: "ICMP 2024"
bookHidden: true
weight: 2
---
# ICOAM 2022
*21st International Congress of Mathematical Physics & Young Researcher Symposium, 28 June - 6 July 2024, Strasbourg, France* 
{{< columns >}}
### [`Conference Website`](https://icmp2024.org/index.html)
<---> 
### [`My Poster`](Kerr_Poster_icmp2024.pdf)
<---> 
{{< /columns >}}


---
title: "Higgs Centre School of Theoretical Physics 2024"
bookHidden: true
weight: 2
---
# Higgs Centre School of Theoretical Physics 2024
*9th edition of the Higgs Centre School of Theoretical Physics, 27–31 May 2024, Higgs Center, University of Edinburgh* 
{{< columns >}}
### [`Conference Website`](https://higgs.ph.ed.ac.uk/workshops/higgs-centre-school-of-theoretical-physics-2024/)
<---> 

<---> 

{{< /columns >}}


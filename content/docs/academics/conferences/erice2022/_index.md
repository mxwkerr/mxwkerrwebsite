---
title: "Topological Methods In Mathematical Physics"
bookHidden: true
weight: 2
---
# Topological Methods In Mathematical Physics
*2-6 September 2022, International School of Mathematics «Guido Stampacchia», Erice, Italy*
{{< columns >}}
### [`Conference Website`](https://staff.matapp.unimib.it/ricca/EMFCSC2022Erice/)
<---> 
### `No Poster`
<---> 
### [`Conference Photo`](ERICE2022.jpg)
{{< /columns >}}
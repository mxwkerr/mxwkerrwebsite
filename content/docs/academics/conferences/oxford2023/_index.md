---
title: "Workshop and school on Complex Lagrangians, Integrable Systems, and Quantization"
bookHidden: true
weight: 2
---
# Workshop and school on Complex Lagrangians, Integrable Systems, and Quantization
*FRG - YEAR 1 meeting, Mathematical Institute, University of Oxford*
*June 5-9 2023*
{{< columns >}}
### [`Conference Website`](https://sites.google.com/view/oxford2023/home)
<---> 
### [`My Poster`](Oxford2023.pdf)
<---> 
### [`Conference Photo`](IMG_9231.jpg)
{{< /columns >}}



---
title: "ICOAM 2022"
bookHidden: true
weight: 2
---
# ICOAM 2022
*6th International Conference on Optical Angular Momentum, 12–17 June 2022, Tampere University, Finland* 
{{< columns >}}
### [`Conference Website`](https://events.tuni.fi/icoam2022/)
<---> 
### [`My Poster`](ICOAM_2022_Poster.pdf)
<---> 
### [`Conference Photo`](ICOAM2022.jpg)
{{< /columns >}}


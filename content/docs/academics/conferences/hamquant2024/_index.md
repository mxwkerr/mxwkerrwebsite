---
title: "Workshop on Hamiltonian Geometry and Quantization 2024"
bookHidden: true
weight: 2
---
# Workshop on Hamiltonian Geometry and Quantization 2024
*July 15 - 19, 2024, The Fields Institute* 
{{< columns >}}
### [`Conference Website`](http://www.fields.utoronto.ca/activities/24-25/Hamiltonian-Geometry)
<---> 

<---> 

{{< /columns >}}


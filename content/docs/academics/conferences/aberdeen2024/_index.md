---
title: "British Topology Meeting"
bookHidden: true
weight: 2
---
# 37th British Topology Meeting
*28-30 August 2024, University of Aberdeen, Scotland*
{{< columns >}}
### [`Conference Website`](https://homepages.abdn.ac.uk/mark.grant/pages/BTM2024/BTM2024.html)
<---> 
### `No Contribution`
<---> 
### [`Conference Photo`](BLANK)
{{< /columns >}}
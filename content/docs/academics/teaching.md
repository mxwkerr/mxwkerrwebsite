---
title: "Teaching"
weight: 3
---

# Teaching

## Lecturing

{{< details title="LM Topology and its Applications (2023)" open=false >}}
I lectured a graduate course on topology and its applications in science and engineering. The beginnings of my lecture notes (covering lectures 1-4) are available [here](CDT_Lecture_Notes.pdf)
{{< /details >}}

## Tutorials

{{< details title="Mathematics for Data Science (2023,2024)" open=false >}}
I TA'd  a four-week [bootcamp on foundational mathematical topics](https://www.birmingham.ac.uk/study/postgraduate/taught/fees-funding/ofs-scholarship/maths-bootcamp) for students entering the [Data Science MSc](https://www.birmingham.ac.uk/postgraduate/courses/taught/computer-science/data-science.aspx) and other similar courses.
{{< /details >}}

{{< details title="Introduction to Topology (2022)" open=false >}}
I ran a serise of companion tutorial sessions for the course *Introduction to Topology* taught by the [CDT in Topological Design](https://www.birmingham.ac.uk/university/colleges/eps/study/phd/cdt/topological-design-cdt/index.aspx)
{{< /details >}}

## Reading Group
{{< details title="Differential Geometry Reading Group (2022)" open=false >}}
I organised a reading group on the basics of intrinsic differential geometry for physics PhD students. I chose to use the book [*Differential Forms*](https://www.worldscientific.com/worldscibooks/10.1142/11058#t=aboutBook) by Guillemin and Haine since it begins from the multilinear algebra perspective instead of the more classical construction of the category of [Smooth Manifolds](https://ncatlab.org/nlab/show/SmoothManifolds).
{{< /details >}}
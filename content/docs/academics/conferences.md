---
title: "Conferences"
weight: 2
---

# Upcoming Conferences

## [British Topology Meeting]({{< relref "aberdeen2024" >}})
*Univerity of Aberdeen*

*August 28 - 30 2024*

---
# Past Conferences

## [Workshop on Hamiltonian Geometry and Quantization]({{< relref "hamquant2024" >}})
*The Fields Institute*

*July 15 - 19 2024*

---
## [ International Congress of Mathematical Physics & YRS]({{< relref "icmp2024" >}})
*Strasbourg, France*

*28 June - 6 July 2024*

---
## [Higgs Centre School of Theoretical Physics 2024]({{< relref "higgs2024" >}})
*Higgs Center, University of Edinburgh*

*May 27-31 2024*

---
## [Workshop and school on Complex Lagrangians, Integrable Systems, and Quantization]({{< relref "oxford2023" >}})
*FRG - YEAR 1 meeting, Mathematical Institute, University of Oxford*

*June 5-9 2023*


---
## [TopPhoto23]({{< relref "topophoton2023" >}})
*Topological Photonics 2023, Madrid, Spain*

*31 May – 2 June 2023*

---
## [Topological Methods In Mathematical Physics]({{< relref "erice2022" >}})
*International School of Mathematics «Guido Stampacchia», Erice, Italy*

*2-6 September 2022*


---
## [ICOAM 2022]({{< relref "icoam2022" >}})
*6th International Conference on Optical Angular Momentum, Tampere University, Finland*

*12–17 June 2022*
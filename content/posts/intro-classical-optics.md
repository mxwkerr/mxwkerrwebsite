---
title: Introduction to Classical Optics
bookToC: false
categories: ["History of Optics"]
date: 2024-02-04
---


What we today call Optics, the study of light, is derived from ancient theories of light and vision, called in ancient Greek ὀπτικός (optikós). In the ancient world the question of what light is was inseparable from the concept of vision. It would be well over a thousand years before any widespread application of our understanding of light would develop (in the form of eyeglasses in 13th cent Italy)[^1], and even longer before the nature of light itself without reference to vision would be seriously investigated[^2]. 

Alongside great empires and their bloody wars; ruins and mythologies, the ancient Mediterranean is also known for its intellectual culture. The time from the birth of Thales of Miletus, who is generally credited as being the first philosopher, to the fall of the western Roman empire would see the rise and fall of many distinct intellectual traditions[^3]main distinct theories of vision that emerged from these traditions. The models that these traditions developed were designed more to fit light and vision into their wider philosophical theories rather than operate as the scientific theories that we would recognise today. Even rudimentary experimental tests of different optical theories would have to wait until as late as 1020 AD when the Arab scholar Ibn Al-Haytham published his book known in Latin as De Aspectibus, which contained descriptions of experiments designed to verify various properties of light and encouraged the reader to perform them for themselves. 

The three optical theories we will discuss: Democritus' Effigies, Plato's Visual Fire and Aristotle's Active Medium were the models that medieval scholars had in mind when they began playing around with arrangements of lenses and we can still find echos of them in the mathematically sophisticated models we use today[^4]. We will see that each model is designed to give a good explanation of a particular aspect of vision while failing to adequately explain others, some are more physical and others metaphysical and, since the investigation of light and vision for the Greeks were one and the same, we will see intriguing comparisons to other human senses and biological arguments for their mechanisms. 

As promised, we will also look at each model's answer to the question "does light travel in straight lines?". After this we will jump hundreds of years into the future to learn about Ibn Al-Haytham and how he began to use experiments to interrogate these models using his real-world experience, a prototype for the procedure still followed by scientists today. 

*This work was supported by the SPIE*

[^1]: The use of “burning glasses” and other basic magnifiers like thread-counting stones are a notable exception, it is
said that Archimedes constructed a burning mirror so large that it was able to set enemy ships on fire and was
used to defend Syracuse from siege in 212 BC.

[^2]:Christiaan Huygens, whose life and work we will discuss in a later article, was a pivotal character in this research,
second only to Isaac Newton in historical importance.

[^3]:The philosophical school known today as Neoplatonism which dominated the late roman empire was subsumed
into Christian theology by the writings of the likes of Augustine. The philosophical dominance of Christian theology
would then obscure the numerous ancient traditions until some re-emerged in the Renaissance.

[^4]: The theory of physics described by Aristotle was the dominant model for the physical world from the 4th century
BC until the Scientific Revolution of the 15th century AD.
---
title: What is a Scientist?
bookToC: false
categories: ["History of Optics"]
date: 2024-02-04
---

What is a scientist? Tautology: When we think of scientists, we should think of all those people throughout history who have practiced science. Those who dedicated some part of their lives to research, either fundamental or applicable, professional or amateur, in what we would today call the Natural Sciences (The modern Social Sciences deserve a separate discussion). In this series of articles, we will attempt to answer this question by focusing by and large on the history of physics, especially the study of light and vision[^1]; beginning with fundamental questions about how we see and culminating (but by no means ending) with modern, Nobel prize winning research. I myself am a physicist and so I won’t be talking much about the connections of light to anatomy, art and other disciplines. Instead, I hope to focus on the ideas themselves, how they were applied in fundamental research and solving real problems and the people who performed this valuable work. Our story will wind together the physical, mathematical and philosophical into, I hope, an interesting tale!   

Along our journey into the history of optics we will encounter a variety of scientists, hopefully including some unexpected characters who you wouldn't think of as such. Beginning around 200 years ago, the training, practicing and culture of science has been steadily professionalised. Before this, many famous scientists often had wealthy patrons, were wealthy individuals themselves or pursued their scientific work in-between their day jobs as priests, physicians, philosopher or patent clerks. The mere fact that people have paying jobs as "scientists" is, in the grand scheme of things, a recent development. 

Given its long history and accessible nature, Optics, as we will see was not, as G. H Hardy might say, a “young man’s game”[^2], with people at all stages of life playing significant roles in our story, each building on the thoughts of those who came before; a better lens grinding technique, a more incisive philosophical distinction, a more sophisticated mathematical construction, discoveries of new planets, all these contribute. Unfortunately, as is the case for histories of many Sciences, optics was for the longest time very much a man's game. A situation we will begin to see change appreciably only when approaching the modern era. 

Now, like any good philosopher (which we should all should, at some extent, aspire to be) we must define our terms, our language, before we go throwing about words like science so carelessly. Before we commence on our expedition through the history of science, we must first answer the question of what science even is... 

*This work was supported by the SPIE*

[^1]: Writing about light presents the unique challenge that much of the common verbiage (to "*see*", "*illuminate*",
"*observe*", "*focus*" etc.) become bad puns.

[^2]: In his famous 1940 essay A Mathematician's Apology, the mathematician G.H. Hardy laments that “*mathematics, more than any other art or science, is a young man's game.*"
---
title: What is Science?
bookToC: false
categories: ["History of Optics"]
date: 2024-02-04
---

Much ink has been spilt over the difficulty of pinning down exactly what this thing called science actually is[^1]. To avoid doing too much violence to the Philosophers of Science (lest they assault us with their epistemological and metaphysical concerns) we will avoid their heavy-duty conceptual models, the history of optics must remain the primary focus of our limited time and cognition. So, in keeping with the common practice of scientists, we will begin with an approximation. 

Let's be pragmatic and try to describe science by looking at the intellectual process of scientists throughout history. Considering the inscrutably creative, individual and collaborative scientific process, in the many forms it has operated throughout history, we can think of scientists as being primarily concerned with three things: phenomena, models and communication. The things that happen in the world that scientists investigate; phenomena. The tools mathematical, diagrammatical, conceptual .etc used to describe phenomena; models. The papers written, lectures delivered, classes taught, conferences attended; communication - for modern science is an intensely collaborative process. Some scientists specialise in modelling phenomena in different ways, some in applying a particular style of model to different phenomena and any scientists' investigations are only useful if communicated to others, be it by paper, picture, poem or theorem[^4]. My goal in writing these articles is to give you some sort of historical flavour, written from the perspective of a working scientist, of what studying a science is like, what sort of intellectual processes it involves, what it is and what it isn't. The hope being that when you need to choose a class in school, job to apply for or degree at university that you can make a better-informed choice. Science is not for everyone and people would do well to remember that it is not the only worthy intellectual pursuit. 

But I am not here to deliver a stale lecture on the historical anatomy of science, oh no, there is in fact an important physical question that we will be centring our thought around. A question that, though it might seem to you borderline trivial to begin with, I hope to show you that it is in fact deep, perplexing and worthy of our time and effort. With it as our guide we will weave together many different strands of historical optical thought and see how different people from different places contributed to its understanding, from the ancient Greeks to the modern day. In the end I hope you will agree with me that sometimes the most inane and trivially obtuse sounding questions, correctly formulated, can be the most interesting. 

Does light travel in straight lines?... 

*This work was supported by the SPIE*

[1^]: Those interested might find Samir Okasha’s *Philosophy of Science: Very Short Introduction* published by Oxford University Press a good starting point for further investigation.
[2^]:  Scientists of the 17th century famously communicated their discoveries to each other as Latin poems which were anagrams of their scientific claims. Once confident in their findings the anagram would be revealed and could be used to determine precedence of a discovery.